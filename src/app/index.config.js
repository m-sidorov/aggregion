export function config ($logProvider, $locationProvider) {
  'ngInject';

  // Enable log
  $logProvider.debugEnabled(true);

  // Enable HTML5 history mode for navigation.
  $locationProvider.html5Mode(true);
}
