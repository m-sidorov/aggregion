import { config } from './index.config';
import { routerConfig } from './router';
import { CatalogService } from './services/catalog';
import { CatalogController } from './catalog/catalog.controller';
import { ItemController } from './item/item.controller';
import { ErrorHandler } from './error-handler'

angular.module('aggregion', [
  'ngAnimate',
  'ngCookies',
  'ngSanitize',
  'ngMessages',
  'ngAria',
  'ngResource',
  'ui.router',
  'ngMaterial'
])
  .config(config)
  .config(routerConfig)
  .service('CatalogService', CatalogService)
  .controller('CatalogController', CatalogController)
  .controller('ItemController', ItemController)
  .run(ErrorHandler);
