export class CatalogService {
  constructor ($resource) {
    'ngInject';

    this.resource = $resource('https://ds.aggregion.com/api/public/catalog/:id');
  }

  getCatalog () {
    return this.resource.query().$promise;
  }

  getItem (id) {
    return this.resource.get({ id }).$promise;
  }
}
