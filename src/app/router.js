export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';

  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/catalog/catalog.html',
      controller: 'CatalogController',
      controllerAs: 'vm',
      resolve: {
        catalog (CatalogService) {
          return CatalogService.getCatalog();
        }
      }
    })
    .state('item', {
      url: '/item/{id:[0-9a-z]{1,}}/',
      templateUrl: 'app/item/item.html',
      controller: 'ItemController',
      controllerAs: 'vm',
      resolve: {
        item (CatalogService, $stateParams) {
          return CatalogService.getItem($stateParams.id);
        }
      }
    })
    .state('error', {
      templateUrl: 'app/error/404.html'
    });

  $urlRouterProvider.otherwise('/');
}
