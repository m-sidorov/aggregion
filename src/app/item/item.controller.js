export class ItemController {
  constructor (item) {
    'ngInject';

    this.item = item;

    this.contentTypes = {
      catalog: 'Catalog',
      licenseCode: 'License code',
      software: 'Software',
      multimedia: 'Multimedia'
    };

    this.getImageUrl = (resourceId) => {
      return `//storage.aggregion.com/api/files/${resourceId}/shared/data`;
    };

    const PLATFORM_NAMES = {
      windows: 'Windows',
      ios: 'iOS',
      android: 'Android',
      macosx: 'OS X'
    };

    this.parsePlatformNames = (platforms) => {
      return platforms.map((platform) => PLATFORM_NAMES[platform] || platform).join(', ');
    };

    this.publishingStatuses = {
        none: 'None',
        inReview: 'In review',
        published: 'Published',
        rejected: 'Rejected'
    };

    this.isNotEmptyObject = (obj) => !!obj && Object.keys(obj).length > 0;
  }
}
