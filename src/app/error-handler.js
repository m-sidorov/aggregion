export function ErrorHandler ($rootScope, $state) {
  'ngInject';

  $rootScope.$on('$stateChangeError', (event, toState, toParams, fromState, fromParams, error) => {
    event.preventDefault();
    console.error(error);
    $state.go('error');
  });
}
